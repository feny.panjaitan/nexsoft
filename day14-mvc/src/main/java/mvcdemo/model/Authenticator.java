package mvcdemo.model;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Authenticator {

    public String authenticate(String username, String password) {
        Pattern p1 = Pattern.compile("^[A-Za-z0-9+_.-]+@[A-Za-z0-9.-]+$");
        Matcher m1 = p1.matcher(username);
        boolean user = m1.matches();

        Pattern passw = Pattern.compile("(?=.*[a-z])(?=.*[A-Z])(?=.?[@#$%^&+=]?)(?=\\S+$).{8,}");
        Matcher m2 = passw.matcher(username);
        boolean pass = m2.matches();

        if (user && pass){
            return "success";
        } else {
            return "failure";
        }
    }
}
